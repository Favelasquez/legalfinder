import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  opt: any[] = JSON.parse(localStorage.getItem('abogados'));
  falsetin: Boolean = false;
  mensj =""
  usuario = ""
  pass = ""
  usuarioss
  passs

  constructor(
    public router: Router,
    public toastController: ToastController
  ) {

  }

  usuarios($event) {
    this.usuario = $event
  }
  passss($event) {
    this.pass = $event
  }

  click() {
    for (let i = 0; i < this.opt.length; i++) {
      if ((this.opt[i].Usuario == this.usuario) && (this.opt[i].Pass == this.pass)) {
        localStorage.setItem('inicio_sesion', JSON.stringify(this.opt[i]));
        console.log(this.opt[i])
        this.router.navigate(['tab5'])
      } else {
        this.falsetin = true
      }
    }
    
    if (this.falsetin == true) {
      this.mensj = "Usuario o Contraseña Incorrectos"
    }
  }

 




}
