import { Component } from '@angular/core';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  nombres = "";
  telefono = "";
  email = "";
  modalidad = "";
  especialidad = "";
  ciudad = "";
  direccion = "";
  experiencia = ""
  trayectoria = "";
  estrellas = "";
  titulo = "";
  universidad = "";
  tarjeta = "";
  foto = "";
  fotoss = ""
  usuario = ""
  usuarioss
  pass = ""
  passss
  valor = ""
  estado = ""
  estadoss
  valorss
  tituloss
  universidadss
  tarjetass
  trayectoriass;
  puntuacion: String;
  experienciass;
  ciudadss;
  direccionss;
  especialidas;
  modalidas;
  nombres1;
  otroladito;
  emaill;
  holas;
  holas1;
  holas2;
  abogados_comerciales = [];
  arr = [];
  opt: any[] = JSON.parse(localStorage.getItem('abogados'));





  constructor() {
    this.holas = localStorage.getItem('abogados')
    console.log(this.opt)

  }

  usuarios($event) {
    this.usuario = $event;
    this.usuarioss = this.usuario
  }
  estados($event) {
    this.estado = $event;
    this.estadoss = this.estado
  }

  passs($event) {
    this.pass = $event;
    this.passss = this.pass
  }
  valors($event) {
    this.valor = $event;
    this.valorss = this.valor
  }

  nombre($event) {
    this.nombres = $event;
    this.nombres1 = this.nombres
  }
  telefonos($event) {
    this.telefono = $event;
    this.otroladito = this.telefono
  }
  emails($event) {
    this.email = $event;
    this.emaill = this.email
  }
  modalidades($event) {
    this.modalidad = $event;
    this.modalidas = this.modalidad
  }
  especialidades($event) {
    this.especialidad = $event;
    this.especialidas = this.especialidad
  }
  ciudads($event) {
    this.ciudad = $event;
    this.ciudadss = this.ciudad
  }
  direccions($event) {
    this.direccion = $event;
    this.direccionss = this.direccion
  }
  experiencias($event) {
    this.experiencia = $event;
    this.experienciass = this.experiencia
  }
  trayectorias($event) {
    this.trayectoria = $event;
    this.trayectoriass = this.trayectoria
  }
  titulos($event) {
    this.titulo = $event;
    this.tituloss = this.titulo
  }
  universidads($event) {
    this.universidad = $event;
    this.universidadss = this.universidad
  }
  tarjetas($event) {
    this.tarjeta = $event;
    this.tarjetass = this.tarjeta
  }

  fotos($event) {
    this.foto = $event;
    this.fotoss = this.foto
  }



  ojo() {
    this.holas = localStorage.getItem('abogados')
    if (this.opt) {
      for (let i = 0; i < this.opt.length; i++) {
        this.abogados_comerciales.push(this.opt[i])
      }
    }

    if (this.experienciass <= 2) {
      this.puntuacion = "2.0";
      this.estrellas = "⭐⭐";
    } else if (this.experienciass == 3) {
      this.puntuacion = "3.0";
      this.estrellas = "⭐⭐⭐";
    } else if (this.experienciass == 4) {
      this.puntuacion = "4.0";
      this.estrellas = "⭐⭐⭐⭐";
    } else if (this.experienciass >= 5) {
      this.puntuacion = "5.0";
      this.estrellas = "⭐⭐⭐⭐⭐";
    }

    if ((this.foto == "") || (this.foto == null)) {
      let otrafoto = "./assets/icon/user.png"
      console.log(typeof (this.foto))
      console.log(otrafoto)
      this.fotoss = otrafoto
    }
    this.abogados_comerciales.push({
      'Nombre': this.nombres1,
      'Telefono': this.otroladito,
      'Email': this.emaill,
      'Modalidad': this.modalidas,
      'Especialidad': this.especialidas,
      'Ciudad': this.ciudadss,
      'Direccion': this.direccionss,
      'Experiencia': this.experienciass,
      'Trayectoria': this.trayectoriass,
      'Puntuacion': this.puntuacion,
      'Estrellas': this.estrellas,
      'Titulo': this.tituloss,
      'Universidad': this.universidadss,
      'Tarjeta': this.tarjetass,
      'Foto': this.fotoss,
      'Usuario': this.usuarioss,
      'Pass': this.passss,
      'Valor': 'COP ' + this.valorss,
      'Estado': this.estadoss


    })

    localStorage.setItem('abogados', JSON.stringify(this.abogados_comerciales));


    this.nombres = "";
    this.telefono = "";
    this.email = "";
    this.modalidad = "";
    this.especialidad = "";
    this.nombres = "";
    this.telefono = "";
    this.email = "";
    this.modalidad = "";
    this.especialidad = "";
    this.ciudad = "";
    this.direccion = "";
    this.experiencia = "";
    this.trayectoria = "";
    this.estrellas = "";
    this.titulo = "";
    this.universidad = "";
    this.tarjeta = "";
    this.foto = "";
    this.usuario = "";
    this.pass = "";
    this.valor = "";
    this.estado = "";


  }



}
