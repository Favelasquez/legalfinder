import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';



@Component({
  selector: 'app-tab5',
  templateUrl: 'tab5.page.html',
  styleUrls: ['tab5.page.scss']
})
export class Tab5Page {
  variable: string;
  abogado: any[] = JSON.parse(localStorage.getItem('inicio_sesion'));
  abogados_comerciales: any[] = JSON.parse(localStorage.getItem('abogados'));
  total_citas: any[]
  nombre
  citas: any[] 

  constructor(
    public router: Router,
  ) {

  
    this.total()

  }


  total() {
    this.nombre = JSON.stringify(this.abogado);
    let sp = this.nombre.split('"')
    console.log(sp[3])
    this.total_citas = JSON.parse(localStorage.getItem('citas'));
    for (let i = 0; i < this.total_citas.length; i++) {
      this.total_citas = this.total_citas.filter((x) => x.Nombre_Abogado == sp[3])
    }
    console.log(this.total_citas);
  }

  pasar(i){
    this.router.navigate(['tab6']);
    console.log(this.total_citas[i]);
    localStorage.setItem('cita_seleccionada', JSON.stringify(this.total_citas[i]));
  }


}
