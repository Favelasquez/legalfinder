import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Subject } from 'rxjs';

import { takeUntil, take } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';


@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
unsubscribeSignal: Subject<void> = new Subject();




  constructor(public navCtrl: NavController, public dialog: MatDialog, public router: Router ) {}

  slides = [
    {
      title: "<b>Bienvenido a Legalfinder</b>",
      description: "<b>LegalFinder</b> es una app diseñada con el fin de que las personas puedan buscar a los mejores abogados según la especialidad que se requiera. Por su parte, le brinda la posibilidad a muchos abogados de trabajar desde cualquier parte del país y manejando sus tiempos. ",
      image: "./assets/law/1slide.jpg",
    },
    {
      title: "<b>Apoyo de Legalfinder a los Abogadoss</b>",
      description: "Al mismo tiempo, con esta app se le busca dar apoyo a los abogados recién egresados, pues es innegable la dificultad que existe hoy en día para entrar al mundo laboral. También, se quiere apoyar a los estudiantes de últimos semestres de la carrera de Derecho para que puedan empezar con su vida laboral y adquirir poco a poco experiencia en el área de su interés. ",
      image: "./assets/law/2slide.jpg",
    },
    {
      title: "<b>Crecimiento de Legalfinder</b>",
      description: "En suma, esta app nace como una forma de revolucionar el mercado laboral de los abogados y de los futuros abogados. Visto que en los últimos años, el impacto del uso de las nuevas tecnologías ha sido sorprendente.  ",
      image: "./assets/law/3slide.jpg",
    }
  ];

  async lazyLoadDetalleAbogados() {
  this.router.navigate(['tab1'])


}
async lazyLoadDetalleRegistro() {
  this.router.navigate(['tab2'])
}

lazyLoadIniciarSesion(){
  this.router.navigate(['tab3'])
}

}


