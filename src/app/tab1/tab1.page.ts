import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  variable: string;
  abogados_comerciales: any[] = JSON.parse(localStorage.getItem('abogados'));
  abogados_comerciales_original: any[] = JSON.parse(localStorage.getItem('abogados'));
  especialidad = "";
  estado = ""
  estadoss
  especialidas;
  cambio = 0


  constructor(
    public router: Router,
    public toastController: ToastController
  ) { this.variable = "Legalfinder"; }

  async presentToastWithOptions() {
    let mensaje = ""
    if (this.especialidas == "Derecho Comercial") {
      mensaje = "<p>Rama del derecho encargada de regular la actividad mercantil que se da entre personas naturales y jurídicas.<br>Contamos con un amplio equipo de abogados con la suficiente experiencia en los campos del Derecho Societario y en temas de:</p> - Reestructuración.<br>- Insolvencias.<br>- Fusiones.<br>- Etc. "
    } else if (this.especialidas == "Derecho Disciplinario") {
      mensaje = "<p>Campo del derecho que disciplina el comportamiento que deben seguir los servidores públicos en ejercicio de sus funciones independientemente de la rama a la que pertenezcan.<br> Nuestros abogados prestan servicios de asesoría o defensa técnica, representación  administrativa y judicial a servidores y a ex-servidores públicos y particulares que ejerzan funciones públicas o administren recursos del Estado en procesos:<p> - Disciplinarios.<br>- De responsabilidad fiscal.<br>- Contencioso administrativo. "
    } else if (this.especialidas == "Derecho Administrativo") {
      mensaje = "Rama del derecho encargada de regular la organización, funcionamiento, poderes y deberes de la administración pública y las relaciones jurídicas entre la administración y otros sujetos."
    } else if (this.especialidas == "Derecho Tributario") {
      mensaje = "Rama del derecho encargada de la: recaudación, gestión y el control de los tributos impuestos a las personas.    "
    } else if (this.especialidas == "Derecho Familia") {
      mensaje = "Rama del derecho que disciplina las relaciones no solo personales, sino también patrimoniales de los miembros que integran una familia."
    } else if (this.especialidas == "Derecho Laboral") {
      mensaje = "Rama del derecho cuyo objeto principal es el de tutelar el trabajo humano realizado por cuenta ajena y bajo una relación de dependencia, pero a cambio de una contraprestación."
    }



    const toast = await this.toastController.create({
      header: this.especialidas,
      message: mensaje,
      position: 'top',
      buttons: [
        {
          text: 'Cerrar',
          role: 'cancel',
        }
      ]
    });
    toast.present();
  }

  ngOnInit() { }

  abogado(i) {
    this.router.navigate(['tab4'])
    console.log(i)
    console.log(this.abogados_comerciales[i])
    localStorage.setItem('abogado_seleccionado', JSON.stringify(this.abogados_comerciales[i]));

  }


  especialidades($event) {
    this.cambio = 1
    this.especialidas = $event

    console.log(this.especialidas)
    this.presentToastWithOptions()
    for (let i = 0; i < this.abogados_comerciales.length; i++) {
      this.abogados_comerciales = this.abogados_comerciales.filter((x) => x.Especialidad == this.especialidas)
      if (this.abogados_comerciales.length > 0) {
        this.abogados_comerciales = this.abogados_comerciales.filter((x) => x.Especialidad == this.especialidas)
      } else {
        this.abogados_comerciales = this.abogados_comerciales_original
      }
    }
  }

  estados($event) {
    this.estadoss = $event
    for (let i = 0; i < this.abogados_comerciales.length; i++) {
      this.abogados_comerciales = this.abogados_comerciales.filter((x) => x.Especialidad == this.especialidas && x.Estado == this.estadoss)
      if (this.abogados_comerciales.length > 0) {
        this.abogados_comerciales = this.abogados_comerciales.filter((x) => x.Especialidad == this.especialidas && x.Estado == this.estadoss)
      } else {
        this.abogados_comerciales = this.abogados_comerciales_original
      }
    }
  }


}
